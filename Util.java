import java.util.Iterator;
import java.util.List;

public class Util {
	public static < E > boolean compareLists(List<E> list1, List<E> list2) {
		if (list1.size() != list2.size()) {
			return false;
		}
		else {
			Iterator<E> list2Iterator = list2.iterator();
			for (E el : list1) {
				if (!el.equals(list2Iterator.next())) {
					return false;
				}
			}
		}
		
		return true;
	}
}
