import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Logger;

public class Grader {

	public static void main(String args[]) throws IOException {
		final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:./*.testcase");
		GraderFileVisitor graderFileVisitor = new GraderFileVisitor(matcher);
		Files.walkFileTree(Paths.get("."), graderFileVisitor);

		List<TestResult> submissionResults = graderFileVisitor.getSubmissionResults();
		printSubmissionResults(submissionResults);

	}

	public static void printSubmissionResults(List<TestResult> submissionResults) {
		ListIterator<TestResult> submissionResultsIterator = submissionResults.listIterator();

		while (submissionResultsIterator.hasNext()) {
			TestResult testResult = submissionResultsIterator.next();
			System.out.println("Test result for " + testResult.getFilePath().getFileName());

			if (testResult.isPassed()) {
				System.out.println("PASSED");
			} else {
				System.out.println("FAILED");
				System.out.println("REASON: " + testResult.getFailReason());

				if (!testResult.getExceptionMessage().equals(""))
					System.out.println("EXCEPTION THROWN: " + testResult.getExceptionMessage());
				
			}
			System.out.println("");
		}

	}
}
