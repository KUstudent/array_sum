import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class GraderFileVisitor extends SimpleFileVisitor<Path> {

	final PathMatcher matcher;

	private List<TestResult> submissionResults;

	public GraderFileVisitor(PathMatcher matcher) {
		super();
		this.matcher = matcher;
		submissionResults = new ArrayList<TestResult>();
	}

	public List<TestResult> getSubmissionResults() {
		return submissionResults;
	}

	public void setSubmissionResults(List<TestResult> submissionResults) {
		this.submissionResults = submissionResults;
	}

	@Override
	public FileVisitResult visitFile(Path testCaseFile, BasicFileAttributes attrs) throws IOException {

		if (matcher.matches(testCaseFile)) {

			List<String> submissionOutput;
			TestResult testResult = new TestResult();

			testResult.setFilePath(testCaseFile);

			try {
				submissionOutput = Submission.arraySum(testCaseFile);
			} catch (Exception e) {
				testResult.setPassed(false);
				testResult.setFailReason("The submission code threw an exception: ");
				testResult.appendToFailReason(e.getStackTrace().toString());
				if (e.getMessage() != null)
					testResult.setExceptionMessage(e.getMessage());

				if (e.getStackTrace() != null)
					testResult.setExceptionStackTrace(e.getStackTrace());

				submissionResults.add(testResult);
				return FileVisitResult.CONTINUE;
			}

			String testCaseFileName = testCaseFile.getFileName().toString();

			String basename = testCaseFile.toFile().getName().substring(0, testCaseFileName.lastIndexOf('.'));

			Path testResultFile = Paths.get(basename.concat(".testresult"));

			List<String> resultLines = Files.readAllLines(testResultFile);

			boolean resultPassed = Util.compareLists(submissionOutput, resultLines);
			testResult.setPassed(resultPassed);

			if (!resultPassed) {
				testResult.setFailReason("Output did not match expected output");
			}
			else {
				testResult.setPassed(true);
			}
			
			submissionResults.add(testResult);
		}
		return FileVisitResult.CONTINUE;
	}

}
