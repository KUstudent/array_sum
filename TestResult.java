import java.nio.file.Path;

public class TestResult {
	private boolean isPassed;
	private String failReason = "";
	private String exceptionMessage = "";
	private StackTraceElement[] exceptionStackTrace;
	private Path filePath;

	public TestResult() {

	}

	public TestResult(boolean isPassed, String failReason) {
		super();
		this.isPassed = isPassed;
		this.failReason = failReason;
	}

	public boolean isPassed() {
		return isPassed;
	}

	public void setPassed(boolean isPassed) {
		this.isPassed = isPassed;
	}

	public String getFailReason() {
		return failReason;
	}

	public void appendToFailReason(String toAppend) {
		failReason.concat(toAppend);
	}

	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}

	public Path getFilePath() {
		return filePath;
	}

	public void setFilePath(Path filePath) {
		this.filePath = filePath;
	}
	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public StackTraceElement[] getExceptionStackTrace() {
		return exceptionStackTrace;
	}

	public void setExceptionStackTrace(StackTraceElement[] exceptionStackTrace) {
		this.exceptionStackTrace = exceptionStackTrace;
	}

}
