import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


class Submission {

	// do not change this message name
    public static List<String> arraySum(Path file) throws IOException {
    	// Implement your solution here (and remove the exception)
    	ArrayList<String> res = new ArrayList<String>();
    	
    	List<String> filecontent = Files.readAllLines(file);
    	
    	Iterator<String> iterator = filecontent.iterator();

    	int n = Integer.parseInt(iterator.next());
    	
    	String[] allnumbers = iterator.next().split(" ");
    	
    	Double sum = 0.0;
    	Double max = Double.NEGATIVE_INFINITY;
    	Double min = Double.POSITIVE_INFINITY;
    	
    	for (String num : allnumbers) {
    		Double numdub = Double.parseDouble(num);
    		sum = sum + numdub;
    		max = numdub > max ? numdub : max;
    		min = numdub < min ? numdub : min;
    	}
    	
    	
    	Double range = max - min;
    	
    	Integer s = sum.intValue();
    	Integer r = range.intValue();

    	res.add( s.toString() );
    	res.add( r.toString() );
    	

//    	System.out.println( String.format("%d", sum));
//    	System.out.println( String.format("%d", range));
    	
    	return res;
    }
}
